package calculoResistencia;

import java.awt.BorderLayout;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.Locale;
import javax.swing.border.EmptyBorder;

public class frmResistencia1 extends javax.swing.JFrame {

    public frmResistencia1() {        
        PanelImagen p = new PanelImagen();
        p.setBorder(new EmptyBorder(5, 5, 5, 5));
        p.setLayout(new BorderLayout(0, 0));
        setContentPane(p);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbBanda1 = new javax.swing.JComboBox<>();
        cbBanda2 = new javax.swing.JComboBox<>();
        cbBanda3 = new javax.swing.JComboBox<>();
        cbBanda4 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jpBanda1 = new javax.swing.JPanel();
        jpBanda2 = new javax.swing.JPanel();
        jpBanda3 = new javax.swing.JPanel();
        jpBanda4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jlBanda1 = new javax.swing.JLabel();
        jlBanda2 = new javax.swing.JLabel();
        jlBanda3 = new javax.swing.JLabel();
        jlBanda4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtValorResistencia = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtValorMin = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtValorMax = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calculos de Resistencia");
        setResizable(false);

        cbBanda1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Marrón", "Rojo", "Naranja", "Amarillo", "Verde", "Azul", "Violeta", "Gris", "Blanco" }));
        cbBanda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBanda1ActionPerformed(evt);
            }
        });

        cbBanda2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Negro", "Marrón", "Rojo", "Naranja", "Amarillo", "Verde", "Azul", "Violeta", "Gris", "Blanco" }));
        cbBanda2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBanda2ActionPerformed(evt);
            }
        });

        cbBanda3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Negro", "Marrón", "Rojo", "Naranja", "Amarillo", "Verde", "Azul", "Oro", "Plateado" }));
        cbBanda3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBanda3ActionPerformed(evt);
            }
        });

        cbBanda4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Marrón", "Rojo", "Amarillo", "Verde", "Azul", "Violeta", "Dorado", "Plateado", "Sin Color" }));
        cbBanda4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBanda4ActionPerformed(evt);
            }
        });

        jLabel1.setText("Banda 1");

        jLabel3.setText("Banda 2");

        jLabel4.setText("Banda 3");

        jLabel5.setText("Banda 4");

        jpBanda1.setBackground(new java.awt.Color(153, 51, 0));
        jpBanda1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jpBanda1Layout = new javax.swing.GroupLayout(jpBanda1);
        jpBanda1.setLayout(jpBanda1Layout);
        jpBanda1Layout.setHorizontalGroup(
            jpBanda1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 49, Short.MAX_VALUE)
        );
        jpBanda1Layout.setVerticalGroup(
            jpBanda1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 207, Short.MAX_VALUE)
        );

        jpBanda2.setBackground(new java.awt.Color(0, 0, 0));
        jpBanda2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jpBanda2Layout = new javax.swing.GroupLayout(jpBanda2);
        jpBanda2.setLayout(jpBanda2Layout);
        jpBanda2Layout.setHorizontalGroup(
            jpBanda2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 49, Short.MAX_VALUE)
        );
        jpBanda2Layout.setVerticalGroup(
            jpBanda2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 268, Short.MAX_VALUE)
        );

        jpBanda3.setBackground(new java.awt.Color(0, 0, 0));
        jpBanda3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jpBanda3Layout = new javax.swing.GroupLayout(jpBanda3);
        jpBanda3.setLayout(jpBanda3Layout);
        jpBanda3Layout.setHorizontalGroup(
            jpBanda3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 49, Short.MAX_VALUE)
        );
        jpBanda3Layout.setVerticalGroup(
            jpBanda3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jpBanda4.setBackground(new java.awt.Color(153, 0, 0));
        jpBanda4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jpBanda4Layout = new javax.swing.GroupLayout(jpBanda4);
        jpBanda4.setLayout(jpBanda4Layout);
        jpBanda4Layout.setHorizontalGroup(
            jpBanda4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 49, Short.MAX_VALUE)
        );
        jpBanda4Layout.setVerticalGroup(
            jpBanda4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 207, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos de Bandas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(0, 0, 0)));

        jlBanda1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlBanda1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlBanda1.setText("1");
        jlBanda1.setToolTipText("");

        jlBanda2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlBanda2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlBanda2.setText("0");

        jlBanda3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlBanda3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlBanda3.setText("x 1");

        jlBanda4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlBanda4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlBanda4.setText("+/- 1%");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jlBanda1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addComponent(jlBanda2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jlBanda3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlBanda4, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jlBanda1, jlBanda2});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlBanda1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlBanda2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlBanda3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlBanda4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos General", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(0, 0, 0)));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Valor de Resistencia: ");

        txtValorResistencia.setToolTipText("");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Valor Minimo: ");

        txtValorMin.setToolTipText("");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Valor Maximo: ");

        txtValorMax.setToolTipText("");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtValorMax, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtValorMin, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtValorResistencia, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtValorResistencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtValorMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtValorMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calculoResistencia/Logo.png"))); // NOI18N

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Resistencia de 2 bandas", "Resistencia de 2 bandas" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(45, 45, 45))
            .addGroup(layout.createSequentialGroup()
                .addGap(181, 181, 181)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cbBanda1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jpBanda1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cbBanda2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jpBanda2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel4)
                    .addComponent(cbBanda3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jpBanda3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cbBanda4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jpBanda4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jComboBox1, 0, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jpBanda1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jpBanda4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jpBanda2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jpBanda3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(6, 6, 6)
                        .addComponent(cbBanda3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(6, 6, 6)
                        .addComponent(cbBanda2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(6, 6, 6)
                        .addComponent(cbBanda1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(6, 6, 6)
                        .addComponent(cbBanda4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbBanda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBanda1ActionPerformed
        int valor = cbBanda1.getSelectedIndex();        
        switch (valor){
            case 0:
                jlBanda1.setText("1");
                jpBanda1.setBackground(new Color(153, 0, 0));
                break;
            case 1:
                jlBanda1.setText("2");
                jpBanda1.setBackground(new Color(255, 0, 0));
                break;
            case 2:
                jlBanda1.setText("3");
                jpBanda1.setBackground(new Color(255, 102, 0));
                break;
            case 3:
                jlBanda1.setText("4");
                jpBanda1.setBackground(new Color(255, 255, 0));
                break;
            case 4:
                jlBanda1.setText("5");
                jpBanda1.setBackground(new Color(0, 204, 0));
                break;
            case 5:
                jlBanda1.setText("6");
                jpBanda1.setBackground(new Color(0, 0, 255));
                break;
            case 6:
                jlBanda1.setText("7");
                jpBanda1.setBackground(new Color(204, 0, 153));
                break;
            case 7:
                jlBanda1.setText("8");
                jpBanda1.setBackground(new Color(153, 153, 153));
                break;
            case 8:
                jlBanda1.setText("9");
                jpBanda1.setBackground(new Color(255, 255, 255));
                break;
                
        }
        calcular();
        calculart();
    }//GEN-LAST:event_cbBanda1ActionPerformed

    private void cbBanda2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBanda2ActionPerformed
        int valor = cbBanda2.getSelectedIndex();
        
        switch (valor){
            case 0:
                jlBanda2.setText("0");
                jpBanda2.setBackground(new Color(0, 0, 0));
                break;
            case 1:
                jlBanda2.setText("1");
                jpBanda2.setBackground(new Color(153, 0, 0));
                break;
            case 2:
                jlBanda2.setText("2");
                jpBanda2.setBackground(new Color(255, 0, 0));
                break;
            case 3:
                jlBanda2.setText("3");
                jpBanda2.setBackground(new Color(255, 102, 0));
                break;
            case 4:
                jlBanda2.setText("4");
                jpBanda2.setBackground(new Color(255, 255, 0));
                break;
            case 5:
                jlBanda2.setText("5");
                jpBanda2.setBackground(new Color(0, 204, 0));
                break;
            case 6:
                jlBanda2.setText("6");
                jpBanda2.setBackground(new Color(0, 0, 255));
                break;
            case 7:
                jlBanda2.setText("7");
                jpBanda2.setBackground(new Color(204, 0, 153));
                break;
            case 8:
                jlBanda2.setText("8");
                jpBanda2.setBackground(new Color(153, 153, 153));
                break;
            case 9:
                jlBanda2.setText("9");
                jpBanda2.setBackground(new Color(255, 255, 255));
                break;
        }
        calcular();
        calculart();
    }//GEN-LAST:event_cbBanda2ActionPerformed

    private void cbBanda3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBanda3ActionPerformed
        int valor = cbBanda3.getSelectedIndex();
        
        switch (valor){
            case 0:
                jlBanda3.setText("x 1");
                jpBanda3.setBackground(new Color(0, 0, 0));
                break;
            case 1:
                jlBanda3.setText("x 10");
                jpBanda3.setBackground(new Color(153, 0, 0));
                break;
            case 2:
                jlBanda3.setText("x 10e2");
                jpBanda3.setBackground(new Color(255, 0, 0));
                break;
            case 3:
                jlBanda3.setText("x 10e3");
                jpBanda3.setBackground(new Color(255, 102, 0));
                break;
            case 4:
                jlBanda3.setText("x 10e4");
                jpBanda3.setBackground(new Color(255, 255, 0));
                break;
            case 5:
                jlBanda3.setText("x 10e5");
                jpBanda3.setBackground(new Color(0, 204, 0));
                break;
            case 6:
                jlBanda3.setText("x 10e6");
                jpBanda3.setBackground(new Color(0, 0, 255));
                break;
            
            case 7:
                jlBanda3.setText("x 10e-1");
                jpBanda3.setBackground(new Color(255,204,0));
                break;
            case 8:
                jlBanda3.setText("x 10e-2");
                jpBanda3.setBackground(new Color(226,220,220));
                break;
        }
        calcular();
        calculart();
    }//GEN-LAST:event_cbBanda3ActionPerformed

    private void cbBanda4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBanda4ActionPerformed
        int valor = cbBanda4.getSelectedIndex();        
        switch (valor){         
            case 0:
                jlBanda4.setText("+/- 1%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(153, 0, 0));
                break;
            case 1:
                jlBanda4.setText("+/- 2%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(255, 0, 0));
                break;
            case 2:
                jlBanda4.setText("+/- 4%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(255, 255, 0));
                break;
            case 3:
                jlBanda4.setText("+/- 0.5%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(0, 204, 0));
                break;
            case 4:
                jlBanda4.setText("+/- 0.25%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(0, 0, 255));
                break;
            case 5:
                jlBanda4.setText("+/- 0.1%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(204, 0, 153));
                break;
            case 6:
                jlBanda4.setText("+/- 5%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(255,204,0));
                break;
            case 7:
                jlBanda4.setText("+/- 10%");
                jpBanda4.setVisible(true);
                jpBanda4.setBackground(new Color(226,220,220));
                break;           
            case 8:
                jlBanda4.setText("+/- 20%");
                jpBanda4.setVisible(false);
                break;
        }
        calculart();
        calcular();
    }//GEN-LAST:event_cbBanda4ActionPerformed
    double valorR = 0.0;
    private void calculart(){
        double valorMin=0.0, valorMax=0.0, t=0.0;
        
        switch(cbBanda4.getSelectedIndex()){
            case 0:
                t = 0.01;
                break;
            case 1:
                t = 0.02;
                break;
            case 2:
                t = 0.04;
                break;
            case 3:
                t = 0.005;
                break;
            case 4:
                t = 0.025;
                break;
            case 5:
                t = 0.001;
                break;
            case 6:
                t = 0.05;
                break;
            case 7:
                t = 0.10;
                break;
            case 8:
                t = 0.20;
                break;            
        }
        valorMin = valorR - (t * valorR);
        valorMax = valorR + (t * valorR);
        
        txtValorMin.setText(conversion(valorMin)+" Ohms");
        txtValorMax.setText(conversion(valorMax)+" Ohms");
    }
    private void calcular(){
        double multi = 0;
        
        int c1 = cbBanda1.getSelectedIndex() + 1 ;
        
        int cifras = Integer.parseInt("" + c1 + "" +cbBanda2.getSelectedIndex());
        
        switch(cbBanda3.getSelectedIndex()){
            case 0:
                multi=1;
                break;
            case 1:
                multi=10;
                break;
            case 2:
                multi=100;
                break;
            case 3:
                multi=1000;
                break;
            case 4:
                multi=10000;
                break;
            case 5:
                multi=100000;
                break;
            case 6:
                multi=1000000;
                break;
            case 7:
                multi=0.1;
                break;
            case 8:
                multi=0.01;
                break;
        }
        valorR = cifras * multi;
        txtValorResistencia.setText(conversion(valorR) + " Ohms");
    }
    
    public String conversion(double valor){
      Locale.setDefault(Locale.US);
      DecimalFormat num = new DecimalFormat("#,###.00");
      return num.format(valor);
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmResistencia1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmResistencia1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmResistencia1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmResistencia1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmResistencia1().setVisible(true);
            }
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbBanda1;
    private javax.swing.JComboBox<String> cbBanda2;
    private javax.swing.JComboBox<String> cbBanda3;
    private javax.swing.JComboBox<String> cbBanda4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel jlBanda1;
    private javax.swing.JLabel jlBanda2;
    private javax.swing.JLabel jlBanda3;
    private javax.swing.JLabel jlBanda4;
    private javax.swing.JPanel jpBanda1;
    private javax.swing.JPanel jpBanda2;
    private javax.swing.JPanel jpBanda3;
    private javax.swing.JPanel jpBanda4;
    private javax.swing.JTextField txtValorMax;
    private javax.swing.JTextField txtValorMin;
    private javax.swing.JTextField txtValorResistencia;
    // End of variables declaration//GEN-END:variables
}
